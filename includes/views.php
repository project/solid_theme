<?php
/**
 * Pre-processors for views.
 */

function solid_preprocess_views_view(&$vars) {

  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('view')));
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('view-' . $vars['name'])));
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('view-id-' . $vars['name'])));
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('view-display-id-' . $vars['display_id'])));
}

/*removes the classes from the list*/
function solid_preprocess_views_view_list(&$vars) {
  //we need to go down to the unformatted preprocess to rip out the individual classes
  solid_preprocess_views_view_unformatted($vars);
}

/*
  views list css classes
  options for renaming classes & removing em
*/
function solid_preprocess_views_view_unformatted(&$vars) {
  //renaming classes

  $row_first = "first";
  $row_last = "last";

  $view = $vars['view'];
  $rows = $vars['rows'];


  $vars['classes_array'] = array();
  $vars['classes'] = array();
  // Set up striping values.
  $count = 0;
  $max = count($rows);
  foreach ($rows as $id => $row) {
    $count++;


    if ($count == 1) {
      $vars['classes'][$id][] = $row_first;
    }
    if ($count == $max) {
      $vars['classes'][$id][] = $row_last;
    }

    if ($row_class = $view->style_plugin->get_row_class($id)) {
      $vars['classes'][$id][] = $row_class;
    }

    // $vars['classes'][$id][] = '';
    if (isset($vars['classes']) && isset($vars['classes'][$id])) {
      $vars['classes_array'][$id] = implode(' ', $vars['classes'][$id]);
    }
    else {
      $vars['classes_array'][$id] = '';
    }

  }
}