<?php
/**
 * Solid is a drupal div killing.
 * no stylesheet loading base theme.
 *
 * Be warned this theme is somewhat opionated about how to
 * create drupal themes.
 * 
 * Some inspiration comes from MortenDK's excelent
 * mothership project.
 *
 */


require_once('includes/views.php');
require_once('includes/pagination.php');

/**
* Uncomment this if you don't use the devel module to rebuild the registry.
*/
// if (theme_get_setting('solid_rebuild_registry')) {
//   system_rebuild_theme_data();
//   drupal_theme_rebuild();
// }

/**
 * Implements HOOK_theme().
 */
function solid_theme() {
  return array(
    'nomarkup' => array(
      'render element' => 'element',
    ),
  );
}

/**
* Removes most of the core and contrib added JS stuff.
* except for the admin menu and drupal.js
*/
function solid_js_alter(&$javascripts) {
  static $processed;
  if (!$processed) {
    $remove_core = theme_get_setting('solid_remove_core_js');
    $remove_contrib = theme_get_setting('solid_remove_contrib_js');
    $jqupdate = module_exists('jquery_update');

    foreach ($javascripts as $file => $value) {
      if ($remove_core) {
        if ((strpos($file, 'misc/') !== FALSE) && strpos($file, 'misc/drupal.js') === FALSE) {
          unset($javascripts[$file]);
        }
      }
      if ($remove_contrib) {
        if ((strpos($file, 'modules/') !== FALSE) && strpos($file, 'admin_menu') === FALSE) {
          if ($jqupdate && strpos($file, 'jquery_update') === FALSE) {
            unset($javascripts[$file]);
          }
        }
      }
    }
    if (!$jqupdate && theme_get_setting('solid_remove_contrib_jquery') && isset($javascripts['misc/jquery.js'])) {
      unset($javascripts['misc/jquery.js']);
    }
  }
}

/**
* Removes most of the core and contrib added CSS stuff.
* except for the admin menu.
*/
function solid_css_alter(&$css) {
  static $processed;
  if (!$processed) {
    $remove_core = theme_get_setting('solid_remove_core_css');
    $remove_contrib = theme_get_setting('solid_remove_contrib_css');
    foreach ($css as $file => $value) {
      if ($remove_contrib) {
        if ((strpos($file, '/modules/') !== FALSE) && strpos($file, 'admin_menu') === FALSE) {
          unset($css[$file]);
        }
      }
      if ($remove_core) {
        if ((strpos($file, 'modules/') !== FALSE) && strpos($file, 'admin_menu') === FALSE) {
         // var_dump($css[$file]);
          unset($css[$file]);
        }
      }
    }
    $processed = TRUE;
  }
}




function solid_class_killer(&$vars) {
  $vars['classes'] = array();;
}

function solid_contextual_links_view_alter(&$element, $items) {
  unset($element['#attached']);
}

/*
 * Move contextual links to their own space directly callable using {{contextual_links}}
 */
function solid_preprocess_entity(&$vars) {
  solid_move_contextual_links($vars);
}


function solid_preprocess_node(&$vars) {
  solid_move_contextual_links($vars);
  // remove all the clas
  $vars['classes'] = array();;


  if (drupal_is_front_page()){
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['type'].'__front';
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['type'] . '__' . $vars['view_mode'].'__front';
  }

  //add a noderef to the list
  if (isset($vars['referencing_field'])) {
    $vars['theme_hook_suggestions'][] = 'node__noderef';
    $vars['theme_hook_suggestions'][] = 'node__noderef__' . $vars['type'];
    $vars['theme_hook_suggestions'][] = 'node__noderef__' . $vars['type'] . '__' . $vars['view_mode'];
  }
  $vars['id_node'] = "";


 
  /*
  remove class from the ul that holds the links
  <ul class="inline links">
  this is generated in the node_build_content() function in the node.module
  */
  if (isset($vars['content']['links']['#attributes']['class'])) {
    $vars['content']['links']['#attributes']['class'] = array_values(array_diff($vars['content']['links']['#attributes']['class'], array('inline')));
  }

  //  remove the class attribute it its empty
  if (isset($vars['content']['links']['#attributes']['class'])) {
    if (isset($vars['content']['links']['#attributes']['class']) && !$vars['content']['links']['#attributes']['class']) {
      unset($vars['content']['links']['#attributes']['class']);
    }
  }
}

// =======================================| COMMENT |========================================
function solid_preprocess_comment(&$vars) {
  $vars['content']['links']['#attributes']['class'] = array_values(array_diff($vars['content']['links']['#attributes']['class'], array('inline')));
}


function solid_preprocess_maintance_page(&$vars) {
  global $theme;
  $vars['path'] = $path = drupal_get_path('theme', $theme);
  $vars['theme_hook_suggestions'][] = 'page__maintenance';
}


function solid_preprocess_menu_block_wrapper(&$vars) {
  $vars['classes_array'] = array();
  if (isset($vars['theme_hook_suggestion'])) {
    $vars['theme_hook_suggestions'][] = $vars['theme_hook_suggestion'];
    unset($vars['theme_hook_suggestion']);
  }
  if (isset($vars['config']['admin_title'])) {
    $machine_name = solid_machine_name($vars['config']['admin_title']);
    $vars['theme_hook_suggestions'][] = 'menu_block_' . $machine_name;
    $vars['theme_hook_suggestions'][] = 'menu_block_wrapper__' . $machine_name;
    $vars['theme_hook_suggestion'] = 'menu_block__' . $machine_name;
  }
}

function solid_preprocess_block(&$vars) {
  $vars['id_block'] = 'id="' . $vars['block_html_id'] . '"';
  $vars['classes_array'] = array($vars['block_html_id']);
  $blockmenus = array('navigation', 'main-menu', 'user-menu', 'menu');
  //add a theme suggestion to block--menu.tpl so we dont have create a ton of blocks with <nav>
  if ($vars['elements']['#block']->module == "system" && in_array($vars['elements']['#block']->module, $blockmenus) || $vars['elements']['#block']->module == "menu_block") {
    $vars['theme_hook_suggestions'][] = 'block__menu';
  }
  $vars['theme_hook_suggestions'][] = "block__" . solid_machine_name($vars['block']->delta);
}

function solid_preprocess_region(&$vars) {
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('region')));
}

function solid_preprocess_page(&$vars) {
  // page--nodetype.tpl.php
  if (isset($vars['node'])) {
    $vars['theme_hook_suggestions'][] = 'page__node__' . $vars['node']->type;
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
  unset($vars['page']['content']['system_main']['default_message']);

  // add suggestions for views pages.
  $viewPage = $view = views_get_page_view();
  if (is_object($viewPage)) {
    $vars['theme_hook_suggestions'][] = 'page__views__page__' . strtolower($view->name);
    $vars['theme_hook_suggestions'][] = 'page__views__' . strtolower($view->name);
  }


  // Remove the block template wrapper from the main content block.
  if (!empty($vars['page']['content']['system_main']) AND isset($vars['page']['content']['system_main']['#theme_wrappers']) AND is_array($vars['page']['content']['system_main']['#theme_wrappers'])) {
    $vars['page']['content']['system_main']['#theme_wrappers'] = array_diff($vars['page']['content']['system_main']['#theme_wrappers'], array('block'));
  }
  /*-
    USER ACCOUNT
    Removes the tabs from user  login, register & password
    fixes the titles to so no more "user account" all over
  */
  switch (current_path()) {
    case 'user':
      $vars['title'] = t('Login');
      unset($vars['tabs']);
      break;
    case 'user/register':
      $vars['title'] = t('New account');
      unset($vars['tabs']);
      break;
    case 'user/password':
      $vars['title'] = t('I forgot my password');
      unset($vars['tabs']);
      break;
  }
  $status = drupal_get_http_header("status");
  if ($status == '404 Not Found') {
    $vars['theme_hook_suggestions'][] = 'page__404';
  }

}

function solid_preprocess_html(&$vars) {

  $vars['html_attributes_array'] = array(
    'lang' => $vars['language']->language,
    'dir' => $vars['language']->dir,
  );
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('html')));
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('logged-in')));
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('no-sidebars')));
  $vars['classes_array'] = array_values(array_diff($vars['classes_array'], array('not-logged-in')));
  $vars['classes_array'] = preg_grep('/^page-node/', $vars['classes_array'], PREG_GREP_INVERT);
}

function solid_process_html(&$vars, $hook) {
  $vars['html_attributes'] = drupal_attributes($vars['html_attributes_array']);
}

/*
  // Purge needless XHTML stuff.
  nathan ftw! -> http://sonspring.com/journal/html5-in-drupal-7
*/
function solid_process_html_tag(&$vars) {
  $el = &$vars['element'];
  // Remove type="..." and CDATA prefix/suffix.
  unset($el['#attributes']['type'], $el['#value_prefix'], $el['#value_suffix']);

  // Remove media="all" but leave others unaffected.
  if (isset($el['#attributes']['media']) && $el['#attributes']['media'] === 'all') {
    unset($el['#attributes']['media']);
  }
}

function solid_menu_tree($variables) {
  return '<ul>' . $variables['tree'] . '</ul>';
}

/*
walk through each menu link and kill the classes we dont want
*/
function solid_menu_link(array $variables) {
  //clean up the classes
  unset($variables['element']['#attributes']['class']);

  $element = $variables['element'];

  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
//  dpr($variables['element']['#attributes']);

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Preprocess a file link, add some extra variables.
 *
 * @param $variables
 */
function solid_preprocess_file_link(&$variables) {

  $file = $variables['file'];

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }
  $mime = array_slice(array_reverse(explode("/", file_icon_path($file))), 0, 1);
  $filedetails = explode('.', $file->filename);
  $content = array();
  $content['file_url'] = file_create_url($file->uri);
  $content['file_name_full'] = $file->filename;
  $content['file_size'] = $file->filesize;
  $content['file_name'] = $filedetails[0];
  $content['file_extension'] = $filedetails[1];
  $content['file_mimetype'] = str_replace('.png', '', $mime[0]);
  $content['file_type'] = $file->type;
  $variables['content'] = $content;
}


/**
 * Move contextual links to their own space, keep the reference for legacy.
 * @param $vars
 */
function solid_move_contextual_links(&$vars) {
  if (isset($vars['title_suffix']['contextual_links'])) {
    $vars['contextual_links'] = $vars['title_suffix']['contextual_links'];
  }
}

function solid_machine_name($string) {
  return preg_replace(array('/[^a-z0-9]/', '/_+/'), '_', strtolower($string));
}

function solid_preprocess_image_formatter(&$variables) {
  if (isset($variables['image_style'])) {
    $variables['theme_hook_suggestions'][] = $variables['theme_hook_original'];
    $variables['theme_hook_suggestions'][] = $variables['theme_hook_original'] . '__' . solid_machine_name($variables['image_style']);
  }
}